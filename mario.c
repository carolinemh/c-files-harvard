#include <cs50.h>
#include <stdio.h>

int height = 0; // Sets variable height to 0.

int main(void) // Start program (main function).
{
    do
    {
        height = get_int("What height should the pyramid be?\n"); // Ask user for input, and gather integer.
    }
    while (height < 1 || height > 8); // Condition.
    for (int i = 1; i <= height; i++) // Loop height amount of times.
    {
        for (int j = 1; j <= height - i; j++) // Height - i amount of spacebars.
        {
            printf(" ");
        }
        for (int j = 1; j <= i; j++) // i amount hashtags.
        {
            printf("#");
        }
        printf("  ");
        
        for (int j = 1; j <= i; j++) // i amount hashtags.
        {
            printf("#");
        }
        
        printf("\n");
    }
}

// i is the amount of times that has been looped through.